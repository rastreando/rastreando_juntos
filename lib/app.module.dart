import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/app.widget.dart';
import 'package:rastreando/pages/home/home.module.dart';
import 'package:rastreando/pages/login/login.module.dart';
import 'package:rastreando/pages/register/register.module.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          module: LoginModule(),
        ),
        ModularRouter(
          '/register',
          module: RegisterModule(),
        ),
        ModularRouter(
          '/home',
          module: HomeModule(),
        ),
      ];

  @override
  Widget get bootstrap => AppWidget();
}
