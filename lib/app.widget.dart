import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart' hide Consumer;
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/user/provider.dart';
import 'package:rastreando/_shared/models/location/provider.dart';
import 'package:rastreando/_shared/theme/provider.dart';
import 'package:rastreando/_shared/theme/theme.config.dart';

class AppWidget extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<AppWidget> {
  final themeProvider = DarkThemeProvider();
  final locationProvider = LocationProvider();

  @override
  void initState() {
    super.initState();
    getCurrentAppTheme();
  }

  void getCurrentAppTheme() async {
    themeProvider.darkTheme = await themeProvider.preferences.getTheme();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: themeProvider),
        ChangeNotifierProvider.value(value: locationProvider),
        ChangeNotifierProvider(create: (_) => UserProvider()),
      ],
      child: Consumer<DarkThemeProvider>(
        builder: (_, themeProvider, __) {
          return MaterialApp(
            title: 'Rastreando Juntos',
            debugShowCheckedModeBanner: false,
            theme: Styles.themeData(themeProvider.darkTheme),
            themeMode: ThemeMode.system,
            navigatorKey: Modular.navigatorKey,
            onGenerateRoute: Modular.generateRoute,
          );
        },
      ),
    );
  }
}
