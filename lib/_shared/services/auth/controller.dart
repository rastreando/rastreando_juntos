import 'dart:async';

import 'package:dio/dio.dart';
import 'package:rastreando/_shared/api.dart' show dio;
import 'package:rastreando/_shared/models/user/user.dart';

class AuthController {
  Future login(final loginData) async {
    var result;

    try {
      Response response = await dio.post(
        '/login',
        data: loginData,
      );

      if (response.statusCode == 200) {
        final u = User.fromJson(response.data);

        result = {
          'ok': true,
          'user': u,
        };

        dio.options.headers = {
          ...dio.options.headers,
          'Authorization': 'Bearer ${u.token}',
        };
      } else {
        result = {
          'ok': false,
          'message': response.data['message'],
        };
      }
    } on DioError catch (e) {
      print(e);
      result = {
        'ok': false,
        'message': 'Internal Server Error',
      };
    }

    return result;
  }

  Future register(final registrationData) async {
    var result;

    try {
      Response response = await dio.post(
        '/register',
        data: registrationData,
      );

      if (response.statusCode == 200) {
        result = {
          'ok': true,
          'message': 'Cadastrado com sucesso',
        };
      } else {
        result = {
          'ok': false,
          'message': response.data['message'],
        };
      }
    } on DioError catch (e) {
      print(e);
      result = {
        'ok': false,
        'message': 'Internal Server Error',
      };
    }

    return result;
  }
}
