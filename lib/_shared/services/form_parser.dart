import 'dart:convert';

import 'package:flutter/services.dart';

class FormServices {
  static Future fromJson(String form) async {
    return rootBundle
        .loadString('assets/forms/$form.json')
        .then((str) => jsonDecode(str));
  }
}
