import 'package:flutter/material.dart';
import 'package:rastreando/_shared/models/terms/term.dart';

void showTerm(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(
        'Termos e Condições',
        textAlign: TextAlign.center,
      ),
      content: Container(
        height: MediaQuery.of(context).size.height * 0.7,
        width: MediaQuery.of(context).size.width * 0.9,
        child: ListView(children: <Widget>[
          Text(
            term,
            textAlign: TextAlign.left,
            softWrap: true,
          ),
        ]),
      ),
    ),
  );
}
