String term = ''' 
Este documento visa registrar a manifestação livre, informada e inequívoca pela qual o Titular concorda com o tratamento de seus dados pessoais para finalidade específica, em conformidade com a Lei nº 13.709 – Lei Geral de Proteção de Dados Pessoais (LGPD).
Ao manifestar sua aceitação para com o presente termo, o Titular consente e concorda que o pesquisador líder do grupo Panacéia Gabriel Alves Castro, CPF 054.399.471-67, e-mail panaceiatecnologia@gmail.com, doravante denominada Controladora, tome decisões referentes ao tratamento de seus dados pessoais e aos dados necessários ao usufruto de serviços ofertados por esse aplicativo, incluindo dados de saúde, bem como realize o tratamento de tais dados, envolvendo operações como as que se referem a coleta, produção, recepção, classificação, utilização, acesso, reprodução, transmissão, distribuição, processamento, arquivamento, armazenamento, eliminação, avaliação ou controle da informação, modificação, comunicação, transferência, difusão ou extração.
Dados Pessoais
A Controladora fica autorizada a tomar decisões referentes ao tratamento e a realizar o tratamento dos seguintes dados pessoais do Titular:
* Nome completo;
* Data de nascimento;
* Número do Cadastro de Pessoas Físicas (CPF);
* Fotografia;
* Estado civil;
* Nível de instrução ou escolaridade;
* Endereço completo;
* Números de telefone, WhatsApp e endereços de e-mail;
* Nome de usuário e senha específicos para uso dos serviços do Controlador;
* Localização georreferenciada;
* Comunicação, verbal e escrita, mantida entre o Titular e o Controlador.
* Dados provenientes da interação com o aplicativo de qualquer natureza.
Além disso, a Controladora fica autorizada a tomar decisões referentes ao tratamento e a realizar o tratamento dos seguintes dados inseridos pelo Titular, com a intenção de obter a prestação dos serviços ofertados pela mesma, como por exemplo, algoritmos de contágio relativos à doenças contagiosas, adaptabilidade inteligente ao usuário, coleta de sintomatologia, dados de localização, entrega de pré-diagnóstico e contato, e serviços de propaganda.
Finalidades do Tratamento dos Dados
O tratamento dos dados pessoais listados neste termo tem as seguintes finalidades:
* Possibilitar que a Controladora identifique e entre em contato com o Titular para fins de acompanhamento e pesquisa relacionadas ao contágio do coronavírus  (COVID-19);
* Possibilitar que a Controladora aplique algoritmos de software para auxílio na autoavaliação de estado de saúde para coronavírus (COVID-19);
* Possibilitar que a Controladora armazene e utilize tais dados para permitir a identificação de potenciais casos de coronavírus (COVID-19);
* Possibilitar que a Controladora utilize tais dados para identificação de redes de contágio de doenças contagiosas;
* Possibilitar que a Controladora aplique formulários de pesquisa de saúde;
* Possibilitar que a Controladora envie ou forneça ao Titular seus produtos e serviços, de forma remunerada ou gratuita.
* Possibilitar que a Controladora estruture, teste, promova e faça propaganda de produtos e serviços, personalizados ou não ao perfil do Titular.
* Possibilitar que a Controladora utilize tais dados em Pesquisas de Mercado;
* Possibilitar que a Controladora utilize tais dados em Pesquisas Científicas;
* Possibilitar que a Controladora utilize tais dados na elaboração de relatórios e emissão de produtos e serviços;
* Possibilitar que a Controladora utilize tais dados para suas peças de Comunicação;
* Possibilitar que a Controladora utilize tais dados para facilitar a prestação de serviços diversos além dos primariamente contratados, desde que o cliente também demonstre interesse em contratar novos serviços;
* Possibilitar que a Controladora utilize tais dados para manter banco de dados de usuários  para facilitar o contatos futuros de qualquer natureza;


Compartilhamento de Dados
A Controladora fica autorizada a compartilhar os dados pessoais do Titular com outros agentes de tratamento de dados, caso seja necessário para as finalidades listadas neste termo, observados os princípios e as garantias estabelecidas pela Lei nº 13.709.
Segurança dos Dados
A Controladora responsabiliza-se pela manutenção de medidas de segurança, técnicas e administrativas aptas a proteger os dados pessoais de acessos não autorizados e de situações acidentais ou ilícitas de destruição, perda, alteração, comunicação ou qualquer forma de tratamento inadequado ou ilícito.
Em conformidade ao art. 48 da Lei nº 13.709, o Controlador comunicará ao Titular e à Autoridade Nacional de Proteção de Dados (ANPD) a ocorrência de incidente de segurança que possa acarretar risco ou dano relevante ao Titular.
Término do Tratamento dos Dados
A Controladora poderá manter e tratar os dados pessoais do Titular durante todo o período em que os mesmos forem pertinentes ao alcance das finalidades listadas neste termo. Dados pessoais anonimizados, sem possibilidade de associação ao indivíduo, poderão ser mantidos por período indefinido.
O Titular poderá solicitar via e-mail ou correspondência ao Controlador, a qualquer momento, que sejam eliminados os dados pessoais não anonimizados do Titular. O Titular fica ciente de que poderá ser inviável ao Controlador continuar o fornecimento de produtos ou serviços ao Titular a partir da eliminação dos dados pessoais.
Direitos do Titular
O Titular tem direito a obter da Controladora, em relação aos dados por ele tratados, a qualquer momento e mediante requisição: I - confirmação da existência de tratamento; II - acesso aos dados; III - correção de dados incompletos, inexatos ou desatualizados; IV - anonimização, bloqueio ou eliminação de dados desnecessários, excessivos ou tratados em desconformidade com o disposto na Lei nº 13.709; V - portabilidade dos dados a outro fornecedor de serviço ou produto, mediante requisição expressa e observados os segredos comercial e industrial, de acordo com a regulamentação do órgão controlador; V - portabilidade dos dados a outro fornecedor de serviço ou produto, mediante requisição expressa, de acordo com a regulamentação da autoridade nacional, observados os segredos comercial e industrial; VI - eliminação dos dados pessoais tratados com o consentimento do titular, exceto nas hipóteses previstas no art. 16 da Lei nº 13.709; VII - informação das entidades públicas e privadas com as quais o controlador realizou uso compartilhado de dados; VIII - informação sobre a possibilidade de não fornecer consentimento e sobre as consequências da negativa; IX - revogação do consentimento, nos termos do § 5º do art. 8º da Lei nº 13.709.
Direito de Revogação do Consentimento
Este consentimento poderá ser revogado pelo Titular, a qualquer momento, mediante solicitação via e-mail ao Controlador.
''';
