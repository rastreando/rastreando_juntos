import 'dart:async';

import 'package:carp_background_location/carp_background_location.dart';
import 'package:dio/dio.dart';
import 'package:rastreando/_shared/api.dart';

class LocationService {
  static LocationDto lastLocation;
  static DateTime lastTimeLocation;
  static LocationManager locationManager = LocationManager.instance;
  static Stream<LocationDto> dtoStream;
  static StreamSubscription<LocationDto> dtoSubscription;

  static void startLocation() async {
    locationManager.interval = 300;
    locationManager.distanceFilter = 1;
    locationManager.notificationTitle = 'Rastreando Juntos Panaceia';
    locationManager.notificationMsg =
        "Estamos coletando sua localizacao, agradescemos sua ajuda!";
    //locationManager.accuracy = LocationAccuracy.BALANCED;
    dtoStream = locationManager.dtoStream;
    dtoSubscription = dtoStream.listen(toServer);

    if (dtoSubscription != null) {
      dtoSubscription.cancel();
    }
    dtoSubscription = dtoStream.listen(toServer);
    await locationManager.start();
  }

  static void onGetCurrentLocation() async {
    LocationDto dto = await locationManager.getCurrentLocation();
    print('Current location: $dto');
  }

  static void toServer(LocationDto dto) async {
    try {
      final response = await dio.post('/locations', data: {
        'data_coleta': DateTime.now().toUtc().toString(),
        'precisao': dto.accuracy,
        'localizacao': {
          'type': 'Point',
          'coordinates': [
            dto.latitude,
            dto.longitude,
          ],
        },
      });
      if (response.statusCode == 200) {
        print(_dtoToString(dto));
      }
    } on DioError catch (e) {
      print(e);
    }
  }

  static void stopLocation() async {
    await locationManager.stop();
  }

  static String _dtoToString(LocationDto dto) =>
      'Location ${dto.latitude}, ${dto.longitude} at ${DateTime.fromMillisecondsSinceEpoch(dto.time ~/ 1)}';
}
