import 'package:flutter/material.dart';
import 'package:rastreando/_shared/models/location/preferences.dart';
import 'package:rastreando/_shared/models/location/services.dart';

class LocationProvider extends ChangeNotifier {
  final preferences = LocationPreference();

  Future<bool> get trackLocalization => preferences.getLocationPreference();

  void startTracking() async {
    await preferences.saveLocationPreference(true);
    LocationService.startLocation();
    notifyListeners();
  }

  void stopTracking() async {
    await preferences.saveLocationPreference(false);
    LocationService.stopLocation();
    notifyListeners();
  }
}
