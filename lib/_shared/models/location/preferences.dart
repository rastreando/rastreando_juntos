import 'package:shared_preferences/shared_preferences.dart';

class LocationPreference {
  Future saveLocationPreference(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('location:allowTrack', value);
  }

  Future<bool> getLocationPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('location:allowTrack') ?? false;
  }
}
