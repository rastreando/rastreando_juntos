class Question {
  String type, label, field;
  List<Answer> answers;

  Question({this.type, this.label, this.answers});

  Question.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    label = json['label'];
    field = json['field'];
    answers =
        json['answers'].map<Answer>((json) => Answer.fromJson(json)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['label'] = this.label;
    data['field'] = this.field;
    data['answers'] = this.answers;
    return data;
  }
}

class Answer {
  String label;
  dynamic value;
  String field;
  bool isPositive;

  Answer({this.label, this.value, this.field, this.isPositive});

  Answer.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    value = json['value'];
    field = json['field'];
    isPositive = json['isPositive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['label'] = this.label;
    data['value'] = this.value;
    data['field'] = this.field;
    data['isPositive'] = this.isPositive;
    return data;
  }
}
