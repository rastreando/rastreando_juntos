import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';

class AdCard extends StatefulWidget {
  @override
  _AdCardState createState() => _AdCardState();
}

class _AdCardState extends State<AdCard> {
  //final _adunitID = 'ca-app-pub-3940256099942544/6300978111';
  final _adunitID = 'ca-app-pub-1628052077804741/4434696151';

  Future<bool> inicializarAd() async {
    //Admob.initialize();
    return await Admob.requestTrackingAuthorization();
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        print('New Admob $adType Ad loaded!');
        break;
      case AdmobAdEvent.opened:
        print('Admob $adType Ad opened!');
        break;
      case AdmobAdEvent.closed:
        print('Admob $adType Ad closed!');
        break;
      case AdmobAdEvent.failedToLoad:
        print('Admob $adType failed to load. :(');
        break;
      case AdmobAdEvent.rewarded:
        print('Reward callback fired. Thanks Andrew!');
        print('Type: ${args['type']}');
        print('Amount: ${args['amount']}');
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: inicializarAd(),
      builder: (context, snapshot) {
        return snapshot.hasData
            ? Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                child: Container(
                  height: 80,
                  child: Card(
                    child: Container(
                      height: 60,
                      width: 468,
                      child: AdmobBanner(
                        adUnitId: _adunitID,
                        adSize: AdmobBannerSize.BANNER,
                        listener:
                            (AdmobAdEvent event, Map<String, dynamic> args) {
                          handleEvent(event, args, 'Banner');
                        },
                        onBannerCreated: (AdmobBannerController controller) {
                          controller.dispose();
                        },
                      ),
                    ),
                  ),
                ),
              )
            : Container();
      },
    );
  }
}
