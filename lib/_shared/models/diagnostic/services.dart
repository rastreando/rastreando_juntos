import 'package:dio/dio.dart';
import 'package:rastreando/_shared/api.dart';
import 'package:rastreando/_shared/models/diagnostic/diagnostic.dart';
import 'package:rastreando/_shared/models/diagnostic/preferences.dart';

class DiagnosticServices {
  static final preferences = DiagnosticPreferences();

  static Future getSpecificDiagnostic(selectedDate) async {
    try {
      final response = await dio.get(
        '/diagnostics/by-user/by-date',
        queryParameters: {
          'date': selectedDate,
        },
      );
      if (response.statusCode == 200) {
        return response.data;
      }
    } on DioError catch (e) {
      print(e);
    }
  }

  static Future<Diagnostic> getLastDiagnostic() async {
    try {
      final response = await dio.get(
        '/diagnostics/by-user',
      );
      if (response.statusCode == 200) {
        if ((response.data as List).isEmpty) {
          return Diagnostic.fromJson({
            'tipo_resultado': '-1',
          });
        }
        final data = response.data
            .map<Diagnostic>((json) => Diagnostic.fromJson(json))
            .toList();
        preferences.saveDiagnostic(data.last);
        return data.last;
      }
      return null;
    } on DioError catch (e) {
      print(e);
      return null;
    }
  }
}
