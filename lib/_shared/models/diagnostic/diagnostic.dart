class Diagnostic {
  String id;
  String resultDate;
  String resultType;

  Diagnostic({
    this.id,
    this.resultDate,
    this.resultType,
  });

  Diagnostic.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    resultType = json['tipo_resultado'];
    resultDate = json['data_resultado'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['tipo_resultado'] = this.resultType;
    data['data_resultado'] = this.resultDate;

    return data;
  }
}
