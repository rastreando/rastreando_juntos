import 'package:rastreando/_shared/models/diagnostic/diagnostic.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DiagnosticPreferences {
  Future saveDiagnostic(Diagnostic diag) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('diagnostic:id', diag.id);
    await prefs.setString('diagnostic:status', diag.resultType);
    await prefs.setString('diagnostic:date', diag.resultDate);
  }

  Future<Diagnostic> getDiagnostic() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final id = prefs.getString('diagnostic:id');
    final status = prefs.getString('diagnostic:status');
    final date = prefs.getString('diagnostic:date');

    return Diagnostic.fromJson({
      '_id': id,
      'tipo_resultado': status,
      'data_resultado': date,
    });
  }

  Future removeDiagnostic() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('diagnostic:status');
    await prefs.remove('diagnostic:date');
  }
}
