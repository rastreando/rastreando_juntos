import 'dart:convert';

import 'package:rastreando/_shared/models/user/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  void saveUser(User user) async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.setString('user:id', user.id);
    await prefs.setString('user:name', user.name);
    await prefs.setString('user:username', user.username);
    await prefs.setString('user:email', user.email);
    await prefs.setInt('user:age', user.age);
    await prefs.setString('user:gender', user.gender);
    await prefs.setString(
        'user:chronicDiseases', jsonEncode(user.chronicDiseases));
    await prefs.setInt('user:residents', user.residents);

    if (user.token != null) refreshToken(user.token);
  }

  Future<User> getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();

    final id = prefs.getString('user:id');
    final name = prefs.getString('user:name');
    final username = prefs.getString('user:username');
    final email = prefs.getString('user:email');
    final age = prefs.getInt('user:age');
    final gender = prefs.getString('user:gender');
    final chronicDiseases = prefs.getString('user:chronicDiseases');
    final residents = prefs.getInt('user:residents');
    final token = prefs.getString('user:token');

    return User.fromJson({
      '_id': id,
      'nome': name,
      'username': username,
      'email': email,
      'idade': age,
      'genero': gender,
      'comorbidades': chronicDiseases,
      'residentes': residents,
      'token': token,
    });
  }

  Future removeUser() async {
    final prefs = await SharedPreferences.getInstance();

    await prefs.remove('user:id');
    await prefs.remove('user:name');
    await prefs.remove('user:username');
    await prefs.remove('user:email');
    await prefs.remove('user:age');
    await prefs.remove('user:gender');
    await prefs.remove('user:chronicDiseases');
    await prefs.remove('user:residents');
    await prefs.remove('user:token');
  }

  Future<String> getToken() {
    return SharedPreferences.getInstance()
        .then((prefs) => prefs.getString('user:token'));
  }

  void refreshToken(String token) {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString('user:token', token);
    });
  }
}
