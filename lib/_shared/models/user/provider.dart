import 'package:flutter/material.dart';
import 'package:rastreando/_shared/models/diagnostic/preferences.dart';
import 'package:rastreando/_shared/models/user/preferences.dart';
import 'package:rastreando/_shared/models/user/user.dart';

class UserProvider with ChangeNotifier {
  User _user;
  final userPreferences = UserPreferences();
  final diagnosticPreferences = DiagnosticPreferences();

  User get user => _user;

  void setUser(User user) {
    _user = user;
    userPreferences.saveUser(user);
    notifyListeners();
  }

  void logout() async {
    _user = null;
    await userPreferences.removeUser();
    await diagnosticPreferences.removeDiagnostic();
  }
}
