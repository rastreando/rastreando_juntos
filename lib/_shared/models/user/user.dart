class User {
  String id;
  String name;
  String username;
  String email;
  int age;
  String gender;
  ChronicDiseases chronicDiseases;
  int residents;
  String password;
  String token;

  User({
    this.id,
    this.name,
    this.username,
    this.email,
    this.age,
    this.gender,
    this.chronicDiseases,
    this.residents,
    this.password,
    this.token,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['nome'];
    username = json['username'];
    email = json['email'];
    age = json['idade'];
    gender = json['genero'];
    chronicDiseases = ChronicDiseases.fromJson(json['comorbidades']);
    residents = json['residentes'];
    password = json['senha'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['nome'] = this.name;
    data['username'] = this.username;
    data['email'] = this.email;
    data['idade'] = this.age;
    data['genero'] = this.gender;
    data['comorbidades'] = this.chronicDiseases.toJson();
    data['residentes'] = this.residents;
    data['senha'] = this.password;
    data['token'] = this.token;

    return data;
  }
}

class ChronicDiseases {
  bool diabetes;
  bool hypertension;
  bool cardiacInsufficiency;
  bool renalInsufficiency;
  bool respiratory;
  bool others;

  ChronicDiseases({
    this.diabetes,
    this.hypertension,
    this.cardiacInsufficiency,
    this.renalInsufficiency,
    this.respiratory,
    this.others,
  });

  ChronicDiseases.fromJson(Map<String, dynamic> json) {
    diabetes = json['diabetes'];
    hypertension = json['hipertensao'];
    cardiacInsufficiency = json['insuficiencia_cardiaca'];
    renalInsufficiency = json['insuficiencia_renal'];
    respiratory = json['insuficiencia_respiratoria'];
    others = json['outras'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['diabetes'] = this.diabetes;
    data['hipertensao'] = this.hypertension;
    data['insuficiencia_cardiaca'] = this.cardiacInsufficiency;
    data['insuficiencia_renal'] = this.renalInsufficiency;
    data['insuficiencia_respiratoria'] = this.respiratory;
    data['outras'] = this.others;
    return data;
  }
}
