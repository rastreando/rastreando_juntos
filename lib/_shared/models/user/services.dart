import 'package:dio/dio.dart';
import 'package:rastreando/_shared/api.dart';
import 'package:rastreando/_shared/models/user/user.dart';

class UserServices {
  static Future getUserById(String id) async {
    var result;

    try {
      Response response = await dio.get(
        '/users/$id',
      );
      if (response.statusCode == 200) {
        result = {
          'status': true,
          'user': User.fromJson(response.data),
        };
      } else {
        result = {
          'status': false,
          'message': response.data['message'],
        };
      }
    } on DioError catch (e) {
      result = {
        'status': false,
        'message': e,
      };
    }

    return result;
  }

  static Future updateUser(String id, Map<String, dynamic> data) async {
    var result;

    try {
      Response response = await dio.put('/users/$id', data: data);
      if (response.statusCode == 200) {
        result = {
          'status': true,
          'user': User.fromJson(response.data),
        };
      } else {
        result = {
          'status': false,
          'message': response.data['message'],
        };
      }
    } on DioError catch (e) {
      result = {
        'status': false,
        'message': e,
      };
    }

    return result;
  }
}
