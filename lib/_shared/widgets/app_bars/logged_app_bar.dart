import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoggedTopBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return AppBar(
      centerTitle: true,
      toolbarHeight: 60,
      title: Container(
        padding: EdgeInsets.all(5),
        height: 50,
        width: size.width / 2,
        child: SvgPicture.asset(
          'assets/images/logo-horizontal.svg',
          color: Colors.white,
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }
}
