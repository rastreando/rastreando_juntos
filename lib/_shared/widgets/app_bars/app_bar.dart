import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TopBar extends StatefulWidget {
  final bool showBackArrow;
  final onBack;

  const TopBar({
    Key key,
    this.showBackArrow = true,
    this.onBack,
  }) : super(key: key);

  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return AppBar(
      centerTitle: true,
      leading: widget.showBackArrow
          ? IconButton(
              icon: Icon(Icons.arrow_back_outlined),
              onPressed: () {
                if (widget.onBack != null) widget.onBack();
                Modular.to.pushNamed('/home');
              },
            )
          : Container(),
      title: Container(
        padding: EdgeInsets.all(5),
        height: 50,
        width: size.width / 2,
        child: SvgPicture.asset(
          'assets/images/logo-horizontal.svg',
          color: Colors.white,
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }
}
