import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart' hide Consumer;
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/user/provider.dart';
import 'package:rastreando/_shared/theme/provider.dart';

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<DarkThemeProvider>(context);

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(context),
          _createDrawerItem(
            icon: Icons.home_outlined,
            text: 'Início',
            onTap: () => Modular.to.pushNamed('/home'),
          ),
          _createDrawerItem(
            icon: Icons.accessibility_new_outlined,
            text: 'Auto Avaliação',
            onTap: () => Modular.to.pushNamed('/home/self-evaluation'),
          ),
          _createDrawerItem(
            icon: Icons.timeline_outlined,
            text: 'Diagnósticos',
            onTap: () => Modular.to.pushNamed('/home/diagnostics'),
          ),
          _createDrawerItem(
            icon: Icons.dynamic_feed_outlined,
            text: 'Notícias',
            onTap: () => Modular.to.pushNamed('/home/news'),
          ),
          Divider(),
          _createDrawerItem(
            icon: themeProvider.darkTheme
                ? Icons.wb_sunny_outlined
                : Icons.brightness_medium_outlined,
            text: 'Modo ${themeProvider.darkTheme ? 'Claro' : 'Escuro'}',
            onTap: () {
              themeProvider.darkTheme = !themeProvider.darkTheme;
            },
          ),
          Divider(
            thickness: 1,
          ),
          _createDrawerItem(
            icon: Icons.login_outlined,
            text: 'Sair',
            onTap: () {
              Provider.of<UserProvider>(context, listen: false).logout();
              Modular.to.pushReplacementNamed('/');
            },
          ),
        ],
      ),
    );
  }

  _createHeader(context) {
    var headerBgColor = Theme.of(context).primaryColor;

    return Consumer<UserProvider>(
      builder: (context, userProvider, _) {
        return UserAccountsDrawerHeader(
          decoration: BoxDecoration(
            color: headerBgColor,
          ),
          accountName: Text(userProvider.user.name),
          accountEmail: Text(userProvider.user.email),
          currentAccountPicture: GestureDetector(
            onTap: () => Modular.to.pushNamed('/home/profile'),
            child: CircleAvatar(
              radius: 30.0,
              backgroundImage: AssetImage(
                'assets/images/default-profile.png',
              ),
              backgroundColor: Colors.white,
            ),
          ),
        );
      },
    );
  }

  Widget _createDrawerItem({
    IconData icon,
    String text,
    GestureTapCallback onTap,
  }) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(
            icon,
          ),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }
}
