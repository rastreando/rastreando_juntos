import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

Dio dio = new Dio(
  new BaseOptions(
    baseUrl: () {
      if (kReleaseMode) {
        return 'https://34.68.212.181';
      } else if (kDebugMode) {
        return 'http://34.68.212.181:3131/api/v1/';
      }
    }(),
    followRedirects: false,
    headers: {
      'Content-Type': 'application/json',
    },
    validateStatus: (status) => status < 500,
  ),
);
