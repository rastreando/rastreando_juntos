import 'package:flutter/material.dart';
import 'package:rastreando/_shared/theme/preferences.dart';

class DarkThemeProvider with ChangeNotifier {
  final preferences = DarkThemePreference();

  bool _darkTheme = false;

  bool get darkTheme => _darkTheme;

  set darkTheme(bool value) {
    _darkTheme = value;
    preferences.toggleDarkTheme(value);
    notifyListeners();
  }
}
