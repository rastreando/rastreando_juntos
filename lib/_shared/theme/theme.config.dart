import 'package:flutter/material.dart';

abstract class Styles {
  static final primaryColor = Color(0xFF27b3FF);
  static final secondaryColor = Color(0xff1565c0);

  static final primaryColorDark = Color(0xFF3262c9);
  static final secondaryColorDark = Color(0xFF597cc2);

  static final _lightTheme = ThemeData(
    brightness: Brightness.light,
    primaryColor: primaryColor,
    accentColor: secondaryColor,
  );

  static final _darkTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: primaryColorDark,
    accentColor: secondaryColorDark,
    textTheme: TextTheme(
      button: TextStyle(
        color: Colors.black26,
      ),
    ),
  );

  static ThemeData themeData(bool isDarkTheme) {
    return isDarkTheme ? _darkTheme : _lightTheme;
  }

  static formFieldDecorator({
    @required context,
    @required label,
    @required hint,
    @required prefixIcon,
    bool forceDisable = false,
    suffixIcon,
  }) {
    final pcolor = Theme.of(context).primaryColor;
    final scolor = Theme.of(context).accentColor;

    return InputDecoration(
      hintText: hint,
      labelText: label,
      contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: pcolor,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: forceDisable ? Colors.grey : scolor,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.grey,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.red,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.red,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      prefixIcon: Icon(prefixIcon),
      suffixIcon: suffixIcon,
    );
  }
}
