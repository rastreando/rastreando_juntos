import 'package:shared_preferences/shared_preferences.dart';

class DarkThemePreference {
  Future toggleDarkTheme(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('theme:isDark', value);
  }

  Future<bool> getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('theme:isDark') ?? false;
  }
}
