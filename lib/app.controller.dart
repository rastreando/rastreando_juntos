import 'package:dio/dio.dart';
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/api.dart';
import 'package:rastreando/_shared/models/user/provider.dart';

// queria fazer um auto login, mas parece que não deu muito certo...
class AppController {
  static Future verifyToken(String token) async {
    final client = dio;

    client.options.headers['authorization'] = 'Bearer $token';

    try {
      final response = await client.post('/auth/verify-token');
      return {
        'ok': response.statusCode == 200,
        'user_id': response.data['id'],
      };
    } on DioError catch (_) {
      return {
        'ok': false,
      };
    }
  }

  static Future<bool> canAutoLogin(context) async {
    final token =
        await Provider.of<UserProvider>(context).userPreferences.getToken();
    if (token != null) {
      final tokenPayload = await AppController.verifyToken(token);
      if (tokenPayload['ok']) {
        return true;
      }
    }

    return false;
  }

  static Future<String> getInitialRoute(context) {
    try {
      return AppController.canAutoLogin(context)
          .then((ok) => ok ? '/home' : '/');
    } catch (e) {
      return Future.value('/');
    }
  }
}
