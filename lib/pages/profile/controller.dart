import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/user/provider.dart';
import 'package:rastreando/_shared/models/user/services.dart';
import 'package:rastreando/_shared/models/user/user.dart';

class ProfileController {
  bool _modified = false;
  Map<String, dynamic> _formData = {};

  get formData => _formData;

  set formData(json) => _formData = json;

  updtFormData(String k, v) {
    _formData = {
      ..._formData,
      k: v,
    };

    if (!_modified) _modified = true;
  }

  fetchUser(id) async {
    return UserServices.getUserById(id);
  }

  updateUser(context) async {
    if (_modified) {
      UserServices.updateUser(_formData['_id'], _formData);
      Provider.of<UserProvider>(context, listen: false)
          .setUser(User.fromJson(_formData));
    }
  }
}
