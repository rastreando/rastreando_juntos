import 'package:flutter/material.dart';
import 'package:rastreando/_shared/theme/theme.config.dart';

class EditableTextField extends StatefulWidget {
  final String label, hint;
  final value, icon, valueComplement, kbType, obscureText, inputFormatters;
  final Function onEditingComplete;

  const EditableTextField({
    Key key,
    @required this.label,
    @required this.hint,
    @required this.icon,
    @required this.value,
    @required this.onEditingComplete,
    this.kbType,
    this.valueComplement,
    this.obscureText,
    this.inputFormatters,
  }) : super(key: key);

  @override
  _EditableTextFieldState createState() => _EditableTextFieldState();
}

class _EditableTextFieldState extends State<EditableTextField> {
  bool _editable = false;

  final _controller = TextEditingController();
  final _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _controller.text = '${widget.value} ${widget.valueComplement ?? ''}'.trim();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final style = Theme.of(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Container(
            padding: EdgeInsets.only(
              left: size.width / 10,
              top: 10,
              bottom: 10,
            ),
            child: TextFormField(
              inputFormatters: widget.inputFormatters,
              controller: _controller,
              obscureText: widget.obscureText ?? false,
              enabled: _editable,
              focusNode: _focusNode,
              keyboardType: widget.kbType ?? TextInputType.text,
              decoration: Styles.formFieldDecorator(
                context: context,
                label: widget.label,
                hint: widget.hint,
                prefixIcon: widget.icon,
              ),
              onEditingComplete: () {
                final sanitezed = _controller.text
                    .replaceAll(' ${widget.valueComplement}', '');
                final text = '$sanitezed ${widget.valueComplement ?? ''}';
                setState(() {
                  _editable = !_editable;
                  widget.onEditingComplete(sanitezed);
                  _controller.value = _controller.value.copyWith(
                    text: text.trim(),
                    selection: TextSelection(
                      baseOffset: text.length,
                      extentOffset: text.length,
                    ),
                    composing: TextRange.empty,
                  );
                });
              },
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: size.width / 20),
          child: IconButton(
            icon: Icon(
              Icons.edit,
            ),
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            color: _editable ? style.accentColor : style.primaryColor,
            onPressed: () {
              setState(() {
                _editable = !_editable;
                if (_editable) {
                  _focusNode.requestFocus();
                }
              });
            },
          ),
        )
      ],
    );
  }
}
