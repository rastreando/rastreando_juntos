import 'package:flutter/material.dart';
import 'package:rastreando/_shared/theme/theme.config.dart';

class EditableDropdownButton extends StatefulWidget {
  final List<Map<String, String>> options;
  final String label, hint;
  final onChange, icon;

  const EditableDropdownButton({
    Key key,
    @required this.options,
    @required this.onChange,
    @required this.label,
    @required this.hint,
    @required this.icon,
  }) : super(key: key);

  @override
  _EditableDropdownButtonState createState() => _EditableDropdownButtonState();
}

class _EditableDropdownButtonState extends State<EditableDropdownButton> {
  String _selected;
  bool _editable = false;

  @override
  void initState() {
    super.initState();
    _selected = widget.options.first['value'];
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final style = Theme.of(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Container(
            padding: EdgeInsets.only(
              left: size.width / 10,
              top: 10,
              bottom: 10,
            ),
            child: DropdownButtonFormField(
              onTap: () => FocusScope.of(context).unfocus(),
              disabledHint: Text(
                widget.options
                    .where((element) => element['value'] == _selected)
                    .first['label'],
              ),
              isDense: true,
              value: _selected,
              items: widget.options
                  .map(
                    (e) => DropdownMenuItem(
                      value: e['value'],
                      child: Center(
                        child: Text(e['label']),
                      ),
                    ),
                  )
                  .toList(),
              onChanged: (_editable
                  ? (v) {
                      setState(() {
                        _selected = v;
                      });
                      widget.onChange(v);
                    }
                  : null),
              decoration: Styles.formFieldDecorator(
                context: context,
                label: widget.label,
                hint: widget.hint,
                prefixIcon: widget.icon,
                forceDisable: !_editable,
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: size.width / 20),
          child: IconButton(
            icon: Icon(
              Icons.edit,
            ),
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            color: _editable ? style.accentColor : style.primaryColor,
            onPressed: () {
              setState(() {
                _editable = !_editable;
              });
            },
          ),
        ),
      ],
    );
  }
}
