import 'package:flutter_modular/flutter_modular.dart' hide Consumer;
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/user/provider.dart';
import 'package:rastreando/pages/profile/profile.widget.dart';

class ProfileModule extends ChildModule {
  static Inject get to => Inject<ProfileModule>.of();

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (_, __) => Consumer<UserProvider>(
            builder: (__, user, _) => ProfileWidget(),
          ),
        ),
      ];
}
