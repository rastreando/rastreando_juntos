import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/user/provider.dart';
import 'package:rastreando/_shared/widgets/app_bars/app_bar.dart';
import 'package:rastreando/_shared/widgets/drawer.dart';
import 'package:rastreando/pages/profile/controller.dart';
import 'package:rastreando/pages/profile/widgets/editable_dropdown_form_field.dart';
import 'package:rastreando/pages/profile/widgets/editable_text_form_field.dart';

class ProfileWidget extends StatelessWidget {
  final ctrl = ProfileController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: TopBar(
          onBack: () {
            ctrl.updateUser(context);
          },
        ),
      ),
      body: _body(context),
    );
  }

  _body(context) {
    final userProvider = Provider.of<UserProvider>(context);
    ctrl.formData = userProvider.user.toJson();

    return ListView(
      children: [
        SizedBox(height: 20),
        Container(
          child: Column(
            children: [
              _header(),
              SizedBox(
                height: 20,
              ),
              _info(context),
              SizedBox(
                height: 40,
              ),
              _deleteAccountButton(),
            ],
          ),
        ),
      ],
    );
  }

  _header() {
    return Column(
      children: [
        Container(
          height: 110,
          width: 110,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: Colors.white,
            image: DecorationImage(
              image: AssetImage(
                'assets/images/default-profile.png',
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          ctrl.formData['username'],
          style: TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  _info(context) {
    final pcolor = Theme.of(context).primaryColor;

    final legend = ({text}) => Padding(
          padding: EdgeInsets.only(bottom: 15, top: 15),
          child: Text(
            text,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: pcolor,
            ),
          ),
        );

    var genderOptions = [
      {'label': 'Outro', 'value': 'O'},
      {'label': 'Masculino', 'value': 'M'},
      {'label': 'Feminino', 'value': 'F'},
    ].where((element) => element['value'] != ctrl.formData['genero']).toList();

    // para garantir que o gênero da pessoa apareça no dropdown
    genderOptions.insert(
      0,
      {
        'label': () {
          switch (ctrl.formData['genero']) {
            case 'M':
              return 'Masculino';
            case 'F':
              return 'Feminino';
            default:
              return 'Outros';
          }
        }(),
        'value': ctrl.formData['genero']
      },
    );

    return Column(
      children: [
        legend(text: 'Informações de contato'),
        EditableTextField(
          label: 'Email',
          hint: 'Atualize seu email',
          icon: Icons.email_outlined,
          value: ctrl.formData['email'],
          kbType: TextInputType.emailAddress,
          onEditingComplete: (v) {
            ctrl.updtFormData('email', v);
          },
        ),
        legend(text: 'Informações básicas'),
        EditableTextField(
          label: 'Nome',
          hint: 'Atualize seu nome',
          icon: Icons.person_outline,
          value: ctrl.formData['nome'],
          kbType: TextInputType.name,
          onEditingComplete: (v) {
            ctrl.updtFormData('nome', v);
          },
        ),
        EditableTextField(
          label: 'Idade',
          hint: 'Atualize sua idade',
          icon: Icons.cake_outlined,
          value: ctrl.formData['idade'],
          valueComplement: 'anos',
          kbType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          onEditingComplete: (v) {
            ctrl.updtFormData('idade', v);
          },
        ),
        EditableTextField(
          label: 'Moradores',
          hint: 'Quantos moram com você?',
          icon: Icons.people,
          value: ctrl.formData['residentes'],
          valueComplement: 'pessoas',
          kbType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          onEditingComplete: (v) {
            ctrl.updtFormData('residentes', v);
          },
        ),
        EditableDropdownButton(
          label: 'Gênero',
          hint: 'Escolha seu Gênero',
          icon: Icons.fingerprint_outlined,
          options: genderOptions,
          onChange: (v) {
            ctrl.updtFormData('genero', v);
          },
        ),
        legend(text: 'Segurança'),
        EditableTextField(
          label: 'Senha',
          hint: 'Atualize sua senha',
          icon: Icons.lock,
          value: '12345678',
          kbType: TextInputType.visiblePassword,
          obscureText: true,
          onEditingComplete: (v) {
            ctrl.updtFormData('senha', v);
          },
        ),
      ],
    );
  }

  _deleteAccountButton() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(36),
            ),
            child: IconButton(
              icon: Icon(Icons.close_outlined),
              onPressed: () {
                print('deletando');
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5, bottom: 30),
            child: Text(
              'Excluir conta',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
