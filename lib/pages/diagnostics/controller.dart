import 'package:rastreando/_shared/models/diagnostic/diagnostic.dart';
import 'package:rastreando/_shared/models/diagnostic/services.dart';

class DiagnosticsController {
  Future getDiagnosticByDate(date) async {
    final filteredDiagnostic =
        await DiagnosticServices.getSpecificDiagnostic(date);

    if (filteredDiagnostic == null) {
      final diag = await DiagnosticServices.preferences.getDiagnostic();
      return diag.resultType == null
          ? DiagnosticServices.getLastDiagnostic()
          : diag;
    }

    return Diagnostic.fromJson(filteredDiagnostic);
  }
}
