import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StatusWidget extends StatefulWidget {
  final String status;

  const StatusWidget({Key key, this.status}) : super(key: key);

  @override
  _StatusWidgetState createState() => _StatusWidgetState();
}

class _StatusWidgetState extends State<StatusWidget> {
  Text getTitle() {
    switch (widget.status) {
      case '1':
        return Text(
          'Contaminado',
          style: TextStyle(color: Colors.red, fontSize: 30),
        );
      case '2':
        return Text(
          'Suspeito',
          style: TextStyle(color: Colors.yellow, fontSize: 30),
        );
      default:
        return Text(
          'Saudável',
          style: TextStyle(color: Colors.green, fontSize: 30),
        );
    }
  }

  Text getLabel() {
    switch (widget.status) {
      case '1':
        return Text(
          '''
- Evite aglomerações;
- Se possível, fique em casa;
- Não compartilhe objetos pessoais;
- Somente procure ajuda médica se houver agravamento dos sintomas;
- Em caso de dúvidas, ligue para o Dique Saúde 136 do Ministério da Saúde.
''',
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.start,
        );
      case '2':
        return Text(
          '''
- Se seus sintomas são leves, é orientado que permaneça em isolamento residencial e somente procure ajuda médica se houver agravamento dos sintomas.
- Em caso de dúvidas, ligue para o Dique Saúde 136 do Ministério da Saúde.
''',
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.start,
        );
      default:
        return Text(
          '''
- Evite aglomerações;
- Se possível, fique em casa;
- Não compartilhe objetos pessoais;
- Somente procure ajuda médica se houver agravamento dos sintomas;
- Em caso de dúvidas, ligue para o Dique Saúde 136 do Ministério da Saúde.
''',
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.start,
        );
    }
  }

  String getPath() {
    switch (widget.status) {
      case '1':
        return 'assets/icons/diagnosis/contaminado.gif';
      case '2':
        return 'assets/icons/diagnosis/suspeito.gif';
      default:
        return 'assets/icons/diagnosis/saudavel.gif';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        getTitle(),
        Image.asset(
          getPath(),
          width: MediaQuery.of(context).size.width / 3,
          height: MediaQuery.of(context).size.height / 3,
        ),
        Container(
          child: Center(
            child: getLabel(),
          ),
          width: MediaQuery.of(context).size.width * 0.8,
        )
      ],
    );
  }
}
