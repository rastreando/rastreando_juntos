import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/pages/diagnostics/diagnostics.widget.dart';

class DiagnosticsModule extends ChildModule {
  static Inject get to => Inject<DiagnosticsModule>.of();

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter('/', child: (_, __) => DiagnosticsWidget()),
      ];
}
