import 'dart:ui';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:rastreando/_shared/widgets/app_bars/app_bar.dart';
import 'package:rastreando/_shared/widgets/drawer.dart';
import 'package:rastreando/pages/diagnostics/controller.dart';
import 'package:rastreando/pages/diagnostics/diagnose.status.widget.dart';

class DiagnosticsWidget extends StatefulWidget {
  @override
  _DiagnosticsState createState() => _DiagnosticsState();
}

class _DiagnosticsState extends State<DiagnosticsWidget> {
  bool modified = false;
  DateTime _selectedDate;
  final controller = DiagnosticsController();

  void _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(2020, 12),
      lastDate: DateTime(2021, 12),
    );
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
    }
  }

  @override
  void initState() {
    setState(() {
      _selectedDate = DateTime.now();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: TopBar(),
      ),
      body: _body(),
    );
  }

  _body() {
    return FutureBuilder(
      future: controller.getDiagnosticByDate(
        _selectedDate.toLocal().toString().substring(0, 10),
      ),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return int.parse(snapshot.data.resultType) >= 0
              ? Container(
                  margin: EdgeInsets.only(top: 16),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Diagnóstico do dia: ${formatDate(
                            DateTime.parse(snapshot.data.resultDate),
                            [dd, '/', mm, '/', yyyy],
                          )}",
                          style: TextStyle(
                            fontSize: 17,
                          ),
                        ),
                        Card(
                          margin: EdgeInsets.all(10),
                          child: Container(
                            margin: EdgeInsets.all(20),
                            child: StatusWidget(
                              status: snapshot.data.resultType,
                            ),
                          ),
                        ),
                        RaisedButton(
                          onPressed: () => _selectDate(context),
                          child: Text('Selecione a Data'),
                        ),
                      ],
                    ),
                  ),
                )
              : AlertDialog(
                  title: Text(
                    'Você ainda não possui nenhum diagnóstico',
                  ),
                );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
