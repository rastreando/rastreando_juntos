import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/_shared/services/auth/controller.dart';

class RegisterController {
  var _formData = {
    'nome': '',
    'username': '',
    'email': '',
    'genero': 'O',
    'enha': '',
    'idade': 0,
    'residentes': 0,
    'comorbidades': {
      'diabetes': false,
      'hipertensao': false,
      'insuficiencia_cardiaca': false,
      'insuficiencia_renal': false,
      'insuficiencia_respiratoria': false,
      'outras': false,
    },
  };

  get formData => _formData;

  updtFormData(String k, v) {
    _formData = {
      ..._formData,
      k: v,
    };
  }

  doRegister(context, GlobalKey<FormState> form) {
    final authCtrl = AuthController();

    if (form.currentState.validate()) {
      form.currentState.save();

      FocusScope.of(context).unfocus();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('registrando...'),
        ),
      );

      authCtrl.register(this.formData).then((response) {
        if (response['ok']) {
          Timer(
            Duration(seconds: 1),
            () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              Modular.to.pushReplacementNamed('/');
            },
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(
                'falha no registro: ${response['message']}',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              backgroundColor: Colors.red,
            ),
          );
        }
      });
    }
  }
}
