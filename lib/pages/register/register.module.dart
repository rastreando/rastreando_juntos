import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/pages/register/register.widget.dart';

class RegisterModule extends ChildModule {
  static Inject get to => Inject<RegisterModule>.of();

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter('/', child: (_, __) => RegisterWidget()),
      ];
}
