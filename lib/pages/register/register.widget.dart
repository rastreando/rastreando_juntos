import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:rastreando/_shared/theme/theme.config.dart';
import 'package:rastreando/_shared/widgets/app_bars/app_bar.dart';
import 'package:rastreando/pages/register/controller.dart';

class RegisterWidget extends StatefulWidget {
  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  bool _hasChronicdisease = false;
  final ctrl = RegisterController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: TopBar(),
      ),
      body: _body(),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Builder(
        builder: (context) {
          final primaryColor = Theme.of(context).primaryColor;

          return Form(
            key: _form,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      top: 20,
                      bottom: 30,
                    ),
                    child: Text(
                      'Insira os dados',
                      style: TextStyle(
                        color: primaryColor,
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  _buildFields(context),
                  _buildButton(context),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  _buildFields(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final field = ({child}) => Padding(
          padding: EdgeInsets.only(
            bottom: 15,
          ),
          child: Container(
            width: size.width / 1.2,
            child: child,
          ),
        );

    final legend = ({text}) => Padding(
          padding: EdgeInsets.only(bottom: 15),
          child: Text(
            text,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        );

    return Column(
      children: [
        legend(text: 'Informações básicas'),
        field(
          child: TextFormField(
            onSaved: (v) {
              ctrl.updtFormData('nome', v);
            },
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Nome',
              hint: 'Digite seu nome',
              prefixIcon: Icons.person_outline,
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.name,
            validator: (value) => value.isEmpty ? 'campo necessário' : null,
          ),
        ),
        field(
          child: TextFormField(
            onSaved: (v) {
              ctrl.updtFormData('username', v);
            },
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Usuário',
              hint: 'Digite seu usuário',
              prefixIcon: Icons.account_circle_outlined,
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.text,
            validator: (value) => value.isEmpty ? 'campo necessário' : null,
          ),
        ),
        field(
          child: TextFormField(
            onSaved: (v) {
              ctrl.updtFormData('email', v);
            },
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'email',
              hint: 'Digite seu email',
              prefixIcon: Icons.email_outlined,
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.emailAddress,
            validator: (value) => value.isEmpty ? 'campo necessário' : null,
          ),
        ),
        field(
          child: TextFormField(
            onSaved: (v) {
              ctrl.updtFormData('idade', int.tryParse(v) ?? 0);
            },
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Idade',
              hint: 'Nos diga sua idade',
              prefixIcon: Icons.cake_outlined,
            ),
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.number,
            validator: (value) => value.isEmpty ? 'campo necessário' : null,
          ),
        ),
        field(
          child: DropdownButtonFormField(
            onTap: () => FocusScope.of(context).unfocus(),
            isExpanded: true,
            isDense: true,
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Gênero',
              hint: 'Escolha seu gênero',
              prefixIcon: Icons.fingerprint_outlined,
            ),
            value: ctrl.formData['genero'] as String,
            items: [
              DropdownMenuItem(
                value: 'O',
                child: Center(
                  child: Text('Outro'),
                ),
              ),
              DropdownMenuItem(
                value: 'M',
                child: Center(
                  child: Text('Masculino'),
                ),
              ),
              DropdownMenuItem(
                value: 'F',
                child: Center(
                  child: Text('Feminino'),
                ),
              ),
            ],
            onChanged: (v) {
              ctrl.updtFormData('genero', v);
            },
          ),
        ),
        legend(text: 'Segurança'),
        field(
          child: TextFormField(
            onSaved: (v) {
              ctrl.updtFormData('senha', v);
            },
            obscureText: true,
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Senha',
              hint: 'Digite sua senha',
              prefixIcon: Icons.lock_outline,
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.visiblePassword,
            validator: (value) => value.isEmpty ? 'campo necessário' : null,
          ),
        ),
        legend(text: 'Informações extras'),
        field(
          child: TextFormField(
            onSaved: (v) {
              ctrl.updtFormData('residentes', int.tryParse(v) ?? 0);
            },
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Moradores',
              hint: 'Quantas pessoas moram com você',
              prefixIcon: Icons.people_outline,
            ),
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.number,
            validator: (value) => value.isEmpty ? 'campo necessário' : null,
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              'Possui alguma doença crônica?',
            ),
            GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: Checkbox(
                value: _hasChronicdisease,
                activeColor: Theme.of(context).primaryColor,
                onChanged: (v) {
                  if (!v) {
                    ctrl.updtFormData('comorbidades', {
                      'diabetes': false,
                      'hipertensao': false,
                      'insuficiencia_cardiaca': false,
                      'insuficiencia_renal': false,
                      'insuficiencia_respiratoria': false,
                      'outras': false,
                    });
                  }
                  setState(() {
                    _hasChronicdisease = v;
                  });
                },
              ),
            )
          ],
        ),
        _buildChronicDiseaseQuestionary(context),
      ],
    );
  }

  _buildChronicDiseaseQuestionary(context) {
    if (!_hasChronicdisease) return Container();

    final primaryColor = Theme.of(context).primaryColor;

    option({String diseaseKey, label}) {
      final cd = ctrl.formData['comorbidades'] as Map;
      final val = cd[diseaseKey] as bool;
      final size = MediaQuery.of(context).size;

      return Container(
        width: size.width / 2.7,
        child: RaisedButton(
          onPressed: () {
            setState(() {
              ctrl.updtFormData('comorbidades', {...cd, diseaseKey: !val});
            });
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          color: !val ? Colors.grey.shade600 : primaryColor,
          child: Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Theme.of(context)
                  .primaryTextTheme
                  .button
                  .color
                  .withOpacity(0.8),
            ),
          ),
        ),
      );
    }

    final row = (d1, d2) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: [
            d1,
            d2,
          ],
        );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          'Selecione uma ou mais dentre as opções',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        row(
          option(diseaseKey: 'diabetes', label: 'Diabetes'),
          option(diseaseKey: 'hipertensao', label: 'Hipertensão'),
        ),
        row(
          option(
              diseaseKey: 'insuficiencia_cardiaca',
              label: 'Insuficiência Cardíaca'),
          option(
              diseaseKey: 'insuficiencia_renal', label: 'Insuficiência Renal'),
        ),
        row(
          option(
              diseaseKey: 'insuficiencia_respiratoria',
              label: 'Problemas Respiratórios'),
          option(diseaseKey: 'outras', label: 'Outras'),
        ),
      ],
    );
  }

  _buildButton(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width / 1.2,
      height: size.height / 15,
      margin: EdgeInsets.only(top: 15, bottom: 36),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        color: primaryColor,
        child: Text(
          'Finalizar',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        onPressed: () {
          ctrl.doRegister(context, _form);
        },
      ),
    );
  }
}
