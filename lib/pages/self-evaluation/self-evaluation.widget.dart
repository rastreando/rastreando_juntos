import 'package:flutter/material.dart';
import 'package:rastreando/_shared/models/question/question.dart';
import 'package:rastreando/_shared/services/form_parser.dart';
import 'package:rastreando/_shared/widgets/app_bars/app_bar.dart';
import 'package:rastreando/_shared/widgets/drawer.dart';
import 'package:rastreando/pages/self-evaluation/controller.dart';
import 'package:rastreando/pages/self-evaluation/widgets/question_builder.dart';

class SelfEvaluationWidget extends StatefulWidget {
  @override
  _SelfEvaluationWidgetState createState() => _SelfEvaluationWidgetState();
}

class _SelfEvaluationWidgetState extends State<SelfEvaluationWidget> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final ctrl = SelfEvaluationController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: TopBar(),
      ),
      body: _wrapper(),
    );
  }

  _wrapper() {
    return FutureBuilder(
      future: FormServices.fromJson('self-evaluation'),
      builder: (context, snapshot) {
        return snapshot.hasData
            ? _body(snapshot.data
                .map<Question>((json) => Question.fromJson(json))
                .toList())
            : Center(child: CircularProgressIndicator());
      },
    );
  }

  _body(List<Question> snapshot) {
    ctrl.fillFormData(snapshot);

    return SingleChildScrollView(
      child: Form(
        key: _form,
        child: Center(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              ListView(
                physics: ScrollPhysics(),
                children: snapshot
                    .map(
                      (q) => QuestionBuilder(
                        q: q,
                        onChange: () {
                          if (q.type == 'binary') {
                            final selected =
                                q.answers.where((e) => e.value == true).first;
                            ctrl.formData[q.field] = selected.isPositive
                                ? selected.value
                                : !selected.value;
                          } else {
                            q.answers.forEach((e) {
                              ctrl.formData[e.field] = e.value;
                            });
                          }
                        },
                      ),
                    )
                    .toList(),
                shrinkWrap: true,
              ),
              _buildButton(context),
            ],
          ),
        ),
      ),
    );
  }

  _buildButton(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width / 1.2,
      height: size.height / 15,
      margin: EdgeInsets.only(top: 15, bottom: 36),
      child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          color: primaryColor,
          child: Text(
            'Finalizar',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          onPressed: () {
            ctrl.sendForm(context, _form);
          }),
    );
  }
}
