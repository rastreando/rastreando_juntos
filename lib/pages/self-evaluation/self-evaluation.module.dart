import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/pages/self-evaluation/self-evaluation.widget.dart';

class SelfEvaluationModule extends ChildModule {
  static Inject get to => Inject<SelfEvaluationModule>.of();

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter('/', child: (_, __) => SelfEvaluationWidget()),
      ];
}
