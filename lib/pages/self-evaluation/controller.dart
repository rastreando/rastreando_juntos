import 'package:flutter/material.dart';
import 'package:rastreando/_shared/api.dart';

class SelfEvaluationController {
  var _formData = {};

  get formData => _formData;

  updtFormData(String k, v) {
    _formData = {
      ..._formData,
      k: v,
    };
  }

  /// Preenche o formData com os campos das questoes de multipla escolha, para
  /// que nao de erro caso nenhum sintoma seja selecionado por exemplo, e para
  /// as questoes binarias é escolhido o valor `null`, desta forma é feita a
  /// checagem pela função `_validadeForm()`
  void fillFormData(questions) {
    questions.forEach((q) {
      if (q.type == 'multi') {
        q.answers.forEach((element) {
          formData[element.field] = false;
        });
      } else {
        formData[q.field] = null;
      }
    });
  }

  sendForm(context, GlobalKey<FormState> form) async {
    if (form.currentState.validate() && _validateForm()) {
      form.currentState.save();

      FocusScope.of(context).unfocus();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Enviando auto avaliação...'),
        ),
      );

      final response = await dio.post('/self-evaluation', data: {
        ..._formData,
        'data_preenchimento': DateTime.now().toUtc().toString(),
      });

      if (response.statusCode == 200) {
        return showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text('Dialog Title'),
            content: Text('This is my content'),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              'falha ao enviar auto avaliação',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            backgroundColor: Colors.red,
          ),
        );
      }
    }
  }

  bool _validateForm() {
    return formData.values.where((v) => v == null).length < 1;
  }
}
