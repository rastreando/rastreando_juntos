import 'package:flutter/material.dart';
import 'package:rastreando/_shared/models/question/question.dart';
import 'package:rastreando/pages/self-evaluation/widgets/answer_builder.dart';

class QuestionBuilder extends StatefulWidget {
  final Question q;
  final onChange;

  QuestionBuilder({this.q, this.onChange});

  @override
  _QuestionBuilderState createState() => _QuestionBuilderState();
}

class _QuestionBuilderState extends State<QuestionBuilder> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Text(
            widget.q.label,
            maxLines: 3,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
        GridView.count(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12),
          physics: NeverScrollableScrollPhysics(),
          childAspectRatio: 4,
          shrinkWrap: true,
          crossAxisCount: 2,
          crossAxisSpacing: 24,
          mainAxisSpacing: 12,
          children: _buildBasedOnType(),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _buildBasedOnType() {
    switch (widget.q.type) {
      case 'binary':
        return _buildBinary();
      default:
        return _buildTypeMany();
    }
  }

  _buildTypeMany() {
    return widget.q.answers
        .map(
          (a) => AnswerBuilder(
            answer: a,
            onTap: (_) {
              widget.onChange();
            },
          ),
        )
        .toList();
  }

  _buildBinary() {
    return widget.q.answers
        .map(
          (a) => AnswerBuilder(
              answer: a,
              onTap: (String clickedLabel) {
                setState(() {
                  final other = widget.q.answers
                      .where((element) => element.label != clickedLabel)
                      .first;
                  if (other.value == true) {
                    other.value = !other.value;
                  }
                });
                widget.onChange();
              }),
        )
        .toList();
  }
}
