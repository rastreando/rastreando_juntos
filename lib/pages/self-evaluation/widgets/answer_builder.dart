import 'package:flutter/material.dart';
import 'package:rastreando/_shared/models/question/question.dart';

class AnswerBuilder extends StatefulWidget {
  final Answer answer;
  final onTap;

  AnswerBuilder({this.answer, this.onTap});

  @override
  _AnswerBuilderState createState() => _AnswerBuilderState();
}

class _AnswerBuilderState extends State<AnswerBuilder> {
  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    return Container(
      child: RaisedButton(
        onPressed: () {
          setState(() {
            widget.answer.value = !widget.answer.value;
            if (widget.onTap != null) {
              widget.onTap(widget.answer.label);
            }
          });
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        color: widget.answer.value ? primaryColor : Colors.grey,
        child: Text(
          widget.answer.label,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Theme.of(context).textTheme.button.color.withOpacity(0.8),
          ),
        ),
      ),
    );
  }
}
