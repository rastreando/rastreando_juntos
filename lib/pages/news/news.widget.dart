import 'package:flutter/material.dart';
import 'package:rastreando/_shared/widgets/app_bars/app_bar.dart';
import 'package:rastreando/_shared/widgets/drawer.dart';
import 'package:rastreando/pages/news/services/fetch.dart';
import 'package:rastreando/pages/news/widgets/news_tile.dart';

class NewsWidget extends StatefulWidget {
  @override
  _NewsWidgetState createState() => _NewsWidgetState();
}

class _NewsWidgetState extends State<NewsWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: TopBar(),
      ),
      body: _body(),
    );
  }

  _body() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      child: FutureBuilder(
        future: fetchHealthNews(),
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ListView.builder(
                  itemCount: snapshot.data.length,
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return NewsTile(
                      imgUrl: snapshot.data[index].urlToImage ??
                          'https://bloximages.chicago2.vip.townnews.com/hjnews.com/content/tncms/custom/image/01d65bdc-75bc-11e9-a6ea-4f05c88a4606.jpg',
                      title: snapshot.data[index].title,
                      desc: snapshot.data[index].description ??
                          'Notícia sem descrição adicional',
                      content: snapshot.data[index].content,
                      newsUrl: snapshot.data[index].url,
                    );
                  },
                )
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
