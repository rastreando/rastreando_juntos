import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/pages/news/news.widget.dart';

class NewsModule extends ChildModule {
  static Inject get to => Inject<NewsModule>.of();

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter('/', child: (_, __) => NewsWidget()),
      ];
}
