import 'dart:async';

import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewsVisualizer extends StatefulWidget {
  final String url;

  NewsVisualizer({@required this.url});

  @override
  _ArticleViewState createState() => _ArticleViewState();
}

class _ArticleViewState extends State<NewsVisualizer> {
  final _controller = Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'Visualizando Notícia',
          ),
        ),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: IconButton(
              onPressed: () => _share(context),
              icon: Icon(Icons.share),
            ),
          ),
        ],
        backgroundColor: Colors.transparent,
      ),
      body: Container(
        height: size.height,
        width: size.width,
        child: WebView(
          initialUrl: widget.url,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
        ),
      ),
    );
  }

  _share(BuildContext context) {
    final text = 'Dê uma olhada nesta notícia: ${widget.url}';

    Share.share(
      text,
      subject: 'notícia interessante',
    );
  }
}
