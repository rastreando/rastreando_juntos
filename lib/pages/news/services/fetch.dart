import 'package:dio/dio.dart';
import 'package:rastreando/_shared/models/news/news.dart';
import 'package:rastreando/pages/news/constants.dart' show NEWS_API_TOKEN;

Future<List<News>> fetchHealthNews() async {
  Dio dio = new Dio(
    new BaseOptions(
      baseUrl: 'https://newsapi.org/v2/',
      followRedirects: false,
      headers: {
        'Content-Type': 'application/json',
      },
      validateStatus: (status) => status < 500,
    ),
  );

  final response = await dio
      .get('top-headlines?apiKey=$NEWS_API_TOKEN&category=health&country=br');

  return (response.data['articles'])
      .map<News>((json) => News.fromJson(json))
      .toList();
}
