import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/pages/login/login.widget.dart';

class LoginModule extends ChildModule {
  static Inject get to => Inject<LoginModule>.of();

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter('/', child: (_, __) => LoginWidget()),
      ];
}
