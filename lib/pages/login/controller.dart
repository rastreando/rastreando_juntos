import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/user/provider.dart';
import 'package:rastreando/_shared/services/auth/controller.dart';

class LoginController {
  var _formData = {
    'username': '',
    'senha': '',
  };

  updtFormData(String k, v) {
    _formData = {
      ..._formData,
      k: v,
    };
  }

  doLogin(context, GlobalKey<FormState> form) async {
    final authCtrl = AuthController();
    final userProvider = Provider.of<UserProvider>(context, listen: false);

    if (form.currentState.validate()) {
      form.currentState.save();

      FocusScope.of(context).unfocus();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('entrando...'),
        ),
      );

      final response = await authCtrl.login(_formData);
      if (response['ok']) {
        userProvider.setUser(response['user']);
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        Modular.to.pushReplacementNamed('/home');
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              'falha no login: ${response['message']}',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            backgroundColor: Colors.red,
          ),
        );
      }
    }
  }
}
