import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart' hide Consumer;
import 'package:rastreando/_shared/models/terms/showTerms.popup.dart';
import 'package:rastreando/_shared/theme/theme.config.dart';
import 'package:rastreando/_shared/widgets/app_bars/app_bar.dart';
import 'package:rastreando/pages/login/controller.dart';

class LoginWidget extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginWidget> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  bool _obscureText = true;
  final ctrl = LoginController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: TopBar(
          showBackArrow: false,
        ),
      ),
      body: _body(),
    );
  }

  _body() {
    return Center(
      child: SingleChildScrollView(
        child: Builder(
          builder: (context) {
            return Form(
              key: _form,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _buildLogoImage(context),
                  _buildFields(context),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                    ),
                  ),
                  _buildButtons(context),
                  FlatButton(
                    color: Colors.white.withOpacity(0),
                    child: Text(
                      "Termos e Condicoes",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    onPressed: () => showTerm(context),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  _buildLogoImage(context) {
    final size = MediaQuery.of(context).size;

    return Container(
      alignment: Alignment.topCenter,
      height: size.height / 3.5,
      decoration: BoxDecoration(color: Colors.transparent),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/images/app_logo.png'),
          ),
        ),
      ),
    );
  }

  _buildFields(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final field = ({child}) => Container(
          width: size.width / 1.2,
          height: size.height / 15,
          child: child,
        );

    return Column(
      children: [
        field(
          child: TextFormField(
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Usuário',
              hint: 'Digite seu usuário',
              prefixIcon: Icons.account_circle_outlined,
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.text,
            onChanged: (v) {
              ctrl.updtFormData('username', v);
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 10,
            bottom: 10,
          ),
        ),
        field(
          child: TextFormField(
            obscureText: _obscureText,
            decoration: Styles.formFieldDecorator(
              context: context,
              label: 'Senha',
              hint: 'Digite sua Senha',
              prefixIcon: Icons.lock_outline,
              suffixIcon: IconButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                icon: Icon(
                  _obscureText ? Icons.visibility : Icons.visibility_off,
                ),
              ),
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            keyboardType: TextInputType.visiblePassword,
            onChanged: (v) {
              ctrl.updtFormData('senha', v);
            },
          ),
        )
      ],
    );
  }

  _buildButtons(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final size = MediaQuery.of(context).size;

    final btn = (text, onPressed) => Container(
          width: size.width / 1.2,
          height: size.height / 15,
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
            color: primaryColor,
            child: Text(
              text,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            onPressed: onPressed,
          ),
        );

    return Column(
      children: [
        btn('Entrar', () {
          ctrl.doLogin(context, _form);
        }),
        SizedBox(
          height: 15,
        ),
        btn('Registrar-se', () {
          Modular.to.pushNamed('/register');
        }),
      ],
    );
  }
}
