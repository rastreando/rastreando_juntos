import 'package:flutter_modular/flutter_modular.dart';
import 'package:rastreando/pages/diagnostics/diagnostics.module.dart';
import 'package:rastreando/pages/home/home.widget.dart';
import 'package:rastreando/pages/news/news.module.dart';
import 'package:rastreando/pages/profile/profile.module.dart';
import 'package:rastreando/pages/self-evaluation/self-evaluation.module.dart';

class HomeModule extends ChildModule {
  static Inject get to => Inject<HomeModule>.of();

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter('/', child: (_, __) => HomeWidget()),
        ModularRouter('/profile', module: ProfileModule()),
        ModularRouter('/news', module: NewsModule()),
        ModularRouter('/diagnostics', module: DiagnosticsModule()),
        ModularRouter('/self-evaluation', module: SelfEvaluationModule()),
      ];
}
