import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/location/preferences.dart';
import 'package:rastreando/_shared/models/location/provider.dart';

import '../controller.dart';

class LocationCard extends StatefulWidget {
  @override
  _LocationCardState createState() => _LocationCardState();
}

class _LocationCardState extends State<LocationCard> {
  LocationPreference local = LocationPreference();
  final controller = HomeController();

  @override
  Widget build(BuildContext context) {
    //final size = MediaQuery.of(context).size;
    final locationProvider = Provider.of<LocationProvider>(context);

    return FutureBuilder(
      future: locationProvider.preferences.getLocationPreference(),
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Column(
                  children: <Widget>[
                    snapshot.data
                        ? Container(
                            child: InkWell(
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 15,
                                  vertical: 10,
                                ),
                                child: Card(
                                  child: InkWell(
                                    splashColor: Colors.blue.withAlpha(30),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "\nParar de compartilhar a localização?\n",
                                          style: TextStyle(
                                            //color: Colors.grey,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15,
                                          ),
                                        ),
                                        RaisedButton(
                                          child: Text("Parar"),
                                          onPressed: () {
                                            locationProvider.stopTracking();
                                          },
                                        ),
                                        Row(
                                          children: [
                                            Container(),
                                            Container(),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 15,
                              vertical: 10,
                            ),
                            child: Card(
                              child: InkWell(
                                splashColor: Colors.blue.withAlpha(30),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "\nRastrear localização?\n",
                                      style: TextStyle(
                                        //color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 15,
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        RaisedButton(
                                          child: Text("Sim"),
                                          onPressed: () {
                                            locationProvider.startTracking();
                                          },
                                        ),
                                        RaisedButton(
                                          child: Text("Nao"),
                                          onPressed: () {
                                            locationProvider.stopTracking();
                                          },
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                  ],
                ),
              )
            : Center(
                child: CircularProgressIndicator(),
              );
      },
    );
  }
}
