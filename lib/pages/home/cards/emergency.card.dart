import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class EmergencyCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    const emergencyNumber = 'tel:192';

    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(15),
            child: Card(
              child: InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: () async => await canLaunch(emergencyNumber)
                    ? await launch(emergencyNumber)
                    : throw 'Não foi possível ligar para $emergencyNumber',
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        children: <Widget>[
                          Text(
                            'Contato de Segurança',
                            style: TextStyle(
                              // color: Colors.grey,
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 10,
                        top: 0,
                        bottom: 0,
                      ),
                      child: SizedBox(
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                              leading: Icon(
                                Icons.local_phone,
                                size: size.width / 10,
                              ),
                              title: Text(
                                'Ligue 190 imediatamente se você estiver tendo alguma emergência.',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: Colors.teal,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        TextButton(
                          child: const Text('LIGAR PARA EMERGÊNCIA'),
                          style: TextButton.styleFrom(
                            primary: Colors.blue,
                            backgroundColor: Colors.transparent,
                            //side: BorderSide(color: Colors.blue, width: 1),
                          ),
                          onPressed: () async => await canLaunch(
                                  emergencyNumber)
                              ? await launch(emergencyNumber)
                              : throw 'Não foi possível ligar para $emergencyNumber',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
