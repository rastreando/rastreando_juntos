import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../controller.dart';

class SelfEvaluationCard extends StatelessWidget {
  final controller = HomeController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: Column(
        children: <Widget>[
          FutureBuilder(
            future: controller.alreadyHaveDiagnostic(),
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Card(
                        child: InkWell(
                          splashColor: Colors.blue.withAlpha(30),
                          onTap: () =>
                              Modular.to.pushNamed('/home/self-evaluation'),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(20),
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      'Diagnóstico',
                                      style: TextStyle(
                                        //color: Colors.grey,
                                        //fontWeight: FontWeight.w400,
                                        fontSize: 15,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 10,
                                ),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      ListTile(
                                        leading: Icon(
                                          snapshot.data
                                              ? Icons.check_box
                                              : Icons.check_box_outline_blank,
                                          color: snapshot.data
                                              ? Colors.green
                                              : Colors.red,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              10,
                                        ),
                                        title: Text(
                                          snapshot.data
                                              ? 'Você já fez sua auto-avaliação hoje'
                                              : 'Você ainda não realizou sua avaliação',
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.teal,
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        subtitle: Text.rich(
                                          TextSpan(
                                            text: snapshot.data
                                                ? 'Continue a manter sua comunidade segura.'
                                                : 'Forneça sua auto-avaliação e nós ajude a manter uma comunidade segura.',
                                          ),
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  TextButton(
                                    child: Text('ACESSAR AUTO AVALIAÇÃO'),
                                    style: TextButton.styleFrom(
                                      primary: Colors.blue,
                                      backgroundColor: Colors.transparent,
                                      //side: BorderSide(color: Colors.blue, width: 1),
                                    ),
                                    onPressed: () => Modular.to.pushNamed(
                                      '/home/self-evaluation',
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: size.width * 0.03,
                                height: size.height * 0.01,
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  : Center(child: CircularProgressIndicator());
            },
          ),
        ],
      ),
    );
  }
}
