import 'package:rastreando/_shared/models/diagnostic/services.dart';

class HomeController {
  alreadyHaveDiagnostic() async {
    final diag = await DiagnosticServices.getSpecificDiagnostic(
      DateTime.now().toLocal().toString().substring(0, 10),
    );
    return diag != null;
  }
}
