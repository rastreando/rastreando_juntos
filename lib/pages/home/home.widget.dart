import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rastreando/_shared/models/ads/cardAd.dart';
import 'package:rastreando/_shared/models/location/provider.dart';
import 'package:rastreando/_shared/widgets/app_bars/logged_app_bar.dart';
import 'package:rastreando/_shared/widgets/drawer.dart';
import 'package:rastreando/pages/home/cards/location.card.dart';
import 'package:rastreando/pages/home/cards/self-evaluation.card.dart';
import 'cards/emergency.card.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  @override
  void initState() {
    super.initState();
    getTrackLocationInfo();
  }

  void getTrackLocationInfo() async {
    final locationProvider =
        Provider.of<LocationProvider>(context, listen: false);
    if (await locationProvider.trackLocalization) {
      locationProvider.startTracking();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: LoggedTopBar(),
      ),
      body: ListView(
        //crossAxisAlignment: CrossAxisAlignment.start,
        //mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SelfEvaluationCard(),
          EmergencyCard(),
          Consumer<LocationProvider>(
            builder: (_, __, ___) => LocationCard(),
          ),
          AdCard(),
        ],
      ),
    );
  }
}
